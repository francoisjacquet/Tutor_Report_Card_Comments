��          <      \       p      q   �   �      (    C     U  �   l                         Small font size Student's Tutor or Homeroom Teacher can enter global Comments for each graded Marking Period. Comments will be displayed on the Report Card. "Name" field is optional. Tutor Report Card Comments Project-Id-Version: Tutor Report Card Comments plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:24+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Majhna velikost pisave Dijakov vzgojitelj lahko vnese globalne komentarje za vsako ocenjeno obdobje ocenjevanja. Komentarji bodo prikazani na poročilu. Polje "Ime" ni obvezno. Komentarji na poročilu mentorja 